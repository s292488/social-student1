package s000000;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import social.NoSuchCodeException;
import social.PersonExistsException;
import social.Social;

public class ClassWithOtherTests {
	
	Social social;

	@Before
	public void setUp() throws PersonExistsException{
		social = new Social();
		social.addPerson("Pudge", "Miles", "Halter");
		social.addPerson("AY", "Alaska", "Young");
		social.addPerson("Colonel", "Chip", "Martin");
		social.addPerson("MFFox", "Takumi", "Hirohito");
		social.addPerson("NotHungarian", "Lara", "Buterskaya");
	}
	
	@Test
	public void testGroups1() throws NoSuchCodeException {
		social.addGroup("Smoking Hole Gang");
		assertEquals(1, social.listOfGroups().size());
		assertTrue(social.listOfGroups().contains("Smoking Hole Gang"));
		
		social.addPersonToGroup("Pudge", "Smoking Hole Gang");
		social.addPersonToGroup("AY", "Smoking Hole Gang");
		social.addPersonToGroup("Colonel", "Smoking Hole Gang");
		social.addPersonToGroup("MFFox", "Smoking Hole Gang");
		
		assertTrue(social.listOfPeopleInGroup("Smoking Hole Gang").contains("Pudge"));
		assertTrue(social.listOfPeopleInGroup("Smoking Hole Gang").contains("AY"));
		assertTrue(social.listOfPeopleInGroup("Smoking Hole Gang").contains("Colonel"));
		assertTrue(social.listOfPeopleInGroup("Smoking Hole Gang").contains("MFFox"));
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void test_noSuchCodeGroups() throws NoSuchCodeException {
		social.addGroup("Smoking Hole Gang");
		assertEquals(1, social.listOfGroups().size());
		assertTrue(social.listOfGroups().contains("Smoking Hole Gang"));
		
		social.addPersonToGroup("Pudge", "Smoking Hole Gang");
		social.addPersonToGroup("AY", "Smoking Hole Gang");
		social.addPersonToGroup("Colonel", "Smoking Hole Gang");
		social.addPersonToGroup("MFFox", "Smoking Hole Gang");
		social.addPersonToGroup("Sara", "Smoking Hole Gang");
	}
	
	@Test
	public void testStats() throws NoSuchCodeException {
		social.addFriendship("Pudge", "AY");
		social.addFriendship("Colonel", "Pudge");
		social.addFriendship("MFFox", "Pudge");
		social.addFriendship("Pudge", "NotHungarian");
		social.addFriendship("MFFox", "Colonel");
		social.addFriendship("MFFox", "NotHungarian");
		social.addFriendship("AY", "Colonel");
		
		assertEquals("Pudge", social.personWithLargestNumberOfFriends());
		
		assertEquals("Pudge", social.personWithMostFriendsOfFriends());
		
		social.addGroup("Smoking Hole Gang");
		
		social.addPersonToGroup("Pudge", "Smoking Hole Gang");
		social.addPersonToGroup("AY", "Smoking Hole Gang");
		social.addPersonToGroup("Colonel", "Smoking Hole Gang");
		social.addPersonToGroup("MFFox", "Smoking Hole Gang");
		
		social.addGroup("Girls");
		social.addPersonToGroup("AY", "Girls");
		social.addPersonToGroup("NotHungarian", "Girls");
		
		social.addGroup("Not Pudge");
		social.addPersonToGroup("MFFox", "Not Pudge");
		social.addPersonToGroup("Colonel", "Not Pudge");
		assertEquals("Smoking Hole Gang", social.largestGroup());
		
		social.addGroup("Them");
		social.addPersonToGroup("Pudge", "Them");
		social.addPersonToGroup("AY", "Them");
		
		assertEquals("AY", social.personInLargestNumberOfGroups());
		
	}
}
