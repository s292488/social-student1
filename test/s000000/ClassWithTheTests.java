package s000000;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import social.NoSuchCodeException;
import social.PersonExistsException;
import social.Social;

public class ClassWithTheTests {
	
	Social social;
	
	@Before
	public void setUp() {
		social = new Social();
	}
	
	@Test
	public void testR1() throws PersonExistsException, NoSuchCodeException {
		social.addPerson("code1", "Name", "Surname");
		social.addPerson("code2", "Alaska", "Young");
		
		assertEquals("code1 Name Surname", social.getPerson("code1"));
		assertEquals("code2 Alaska Young", social.getPerson("code2"));
	}
	
	@Test(expected = PersonExistsException.class)
	public void testPersonAlreadyExists() throws PersonExistsException {
		social.addPerson("code1", "Alaska", "Young");
		social.addPerson("code1", "Alaska", "Young");
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void testNoSuchCode() throws PersonExistsException, NoSuchCodeException {
		social.addPerson("code1", "Name", "Surname");
		social.addPerson("code2", "Alaska", "Young");
		
		assertEquals("code1 Name Surname", social.getPerson("code3"));
	}
	
	@Test
	public void testR2_Friends() throws PersonExistsException, NoSuchCodeException{
		social.addPerson("Pudge", "Miles", "Halter");
		social.addPerson("AY", "Alaska", "Young");
		social.addPerson("TheEagle", "Mr.", "Starnes");
		
		social.addFriendship("Pudge", "AY");
		Collection<String> alaskasFriends = social.listOfFriends("AY");
		assertEquals(1, alaskasFriends.size());
		assertTrue(alaskasFriends.contains("Pudge"));
		assertTrue(social.listOfFriends("Pudge").contains("AY"));
		
		Collection<String> eaglesFriends = social.listOfFriends("TheEagle");
		assertEquals(0, eaglesFriends.size());
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void testR2_NoSuchCode() throws NoSuchCodeException, PersonExistsException {
		social.listOfFriends("Who?");
	}
	
	@Test
	public void test_FriendsOfFriends() throws PersonExistsException, NoSuchCodeException {
		social.addPerson("Pudge", "Miles", "Halter");
		social.addPerson("AY", "Alaska", "Young");
		social.addPerson("Colonel", "Chip", "Martin");
		social.addPerson("MFFox", "Takumi", "Hirohito");
		social.addPerson("NotHungarian", "Lara", "Buterskaya");
		
		social.addFriendship("Pudge", "AY");
		social.addFriendship("NotHungarian", "MFFox");
		social.addFriendship("AY", "Colonel");
		social.addFriendship("AY", "MFFox");
		social.addFriendship("NotHungarian", "Pudge");
		
		Collection<String> friendsOfPudgesFriends = social.friendsOfFriends("Pudge");
		assertTrue(friendsOfPudgesFriends.contains("Colonel"));
		assertTrue(friendsOfPudgesFriends.contains("MFFox"));
		assertFalse(friendsOfPudgesFriends.contains("Pudge"));
	}
	
	@Test
	public void test_FriendsOfFriendsNoRepetitions() throws PersonExistsException, NoSuchCodeException {
		social.addPerson("Pudge", "Miles", "Halter");
		social.addPerson("AY", "Alaska", "Young");
		social.addPerson("Colonel", "Chip", "Martin");
		social.addPerson("MFFox", "Takumi", "Hirohito");
		social.addPerson("NotHungarian", "Lara", "Buterskaya");
		
		social.addFriendship("Pudge", "AY");
		social.addFriendship("NotHungarian", "MFFox");
		social.addFriendship("AY", "Colonel");
		social.addFriendship("AY", "MFFox");
		social.addFriendship("NotHungarian", "Pudge");
		
		Collection<String> friendsOfPudgesFriends = social.friendsOfFriendsNoRepetition("Pudge");
		assertTrue(friendsOfPudgesFriends.contains("Colonel"));
		assertTrue(friendsOfPudgesFriends.contains("MFFox"));
		assertFalse(friendsOfPudgesFriends.contains("Pudge"));
	}
}
